﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;

namespace MinimalExcel
{
    class ExcelControl
    {
        ExcelControl()
        {
            // COM Objekte deklarieren und leer initialisieren
            Excel.Application   excelApp = null;
            Excel.Workbooks     excelWorkbooks = null;
            Excel.Workbook      excelWorkbook = null;
            Excel.Worksheet     excelSheet = null;
            Excel.Range         excelBereich = null;

            // Excel COM Objekte erzeugen
            excelApp = new Excel.Application();
            excelWorkbooks = excelApp.Workbooks;
            excelApp.Visible = true;
            excelWorkbook = excelWorkbooks.Add();
            excelSheet = excelApp.ActiveSheet;

            excelSheet = (Excel.Worksheet)excelApp.ActiveSheet;
            excelSheet.Cells[1, "A"] = "Hallo Welt";
            excelSheet.Cells[2, "A"] = "1";
            excelSheet.Cells[3, "A"] = "=A2+2";

            excelBereich = excelSheet.get_Range("A1", "B3");
            excelBereich.Borders[Excel.XlBordersIndex.xlEdgeBottom].Color = Color.Green;

            // https://docs.microsoft.com/de-de/dotnet/csharp/programming-guide/interop/walkthrough-office-programming

            Console.WriteLine("Excel App erzeugt!");
            Console.ReadKey();

            if (excelApp != null) excelApp.Quit();

            // Achtung: Es kann sein, dass Excel sich durch diesen Aufruf nicht schliesst (Zombie Prozess im Task Manager)
            // Dann müssen Sie nachhelfen, dass alle COM Objekte freigegeben werden (Branch GarbageCollection)

        }


        static void Main(string[] args)
        {
            new ExcelControl();
        }
    }
}